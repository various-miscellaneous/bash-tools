#!/bin/bash
# MIT License
#
# Copyright (c) 2019 Magnus Jørgensen
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

PORT=$1
REMOTE=$2

write_hint()
{
	echo "usage: TunnelLocalPortToRemote port [user@]remote"
	echo "example:"
	echo "./TunnelLocalPortToRemote.sh 8888 user@192.168.0.1"
}

if [ "$PORT" == "" ]; then
	echo "Positional parameter 1 shall contain port number." 
	write_hint  
	exit 1
fi
if [ "$REMOTE" == "" ]; then
	echo "Positional parameter 2 shall contain port remote."   
	write_hint
	exit 1
fi

echo 'Tunnelling port:' $PORT ' from this computer to:' $REMOTE

ssh -N -R $PORT:localhost:$PORT $REMOTE

